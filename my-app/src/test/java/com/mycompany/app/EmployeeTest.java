package com.mycompany.app;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

public class EmployeeTest {
	
	private static EmployeeDAO mockeDAO;
	
	@BeforeClass
	public static void mockData() {
		mockeDAO = Mockito.mock(EmployeeDAO.class);
		
		List<EmployeeBean> list = new ArrayList<EmployeeBean>();
		
		list.add(new EmployeeBean(1,"John",10000.0F));
		list.add(new EmployeeBean(2,"Adam",20000.0F));
		list.add(new EmployeeBean(3,"Rahul",10000.0F));
		list.add(new EmployeeBean(4,"Ram",20000.0F));
		list.add(new EmployeeBean(5,"Vikas",15000.0F));
		list.add(new EmployeeBean(6,"Rohit",25000.0F));
		list.add(new EmployeeBean(7,"Aman",10000.0F));
		list.add(new EmployeeBean(8,"Priyam",15000.0F));
		list.add(new EmployeeBean(9,"Sidhant",25000.0F));
		list.add(new EmployeeBean(10,"Sudarshar",20000.0F));
		list.add(new EmployeeBean(11,"Rajat",10000.0F));
		list.add(new EmployeeBean(12,"Chinmay",15000.0F));
		
		Mockito.when(mockeDAO.readData()).thenReturn(list);
		
	}
	
	@Test
	public void getCountTest() {
		
		Assert.assertEquals(4, new EmployeeDAO().getCount(mockeDAO.readData(), 10000.0F));
		
	}
	
	@Test
	public void getEmployeeTest() {
	
		EmployeeBean eb = new EmployeeBean(1, "John", 10000.0f);
		boolean flag = eb.equals(new EmployeeDAO().getEmployee(0));
		
		Assert.assertEquals(true, flag);
		
		
	}
	
	
	@Test
	public void getTotSalTest() {
		
//		List<EmployeeBean> list = new ArrayList<EmployeeBean>();
//		list.add(new EmployeeBean(1,"John",10000.0F));
//		list.add(new EmployeeBean(2,"Adam",20000.0F));
//		list.add(new EmployeeBean(3,"Rahul",10000.0F));
//		list.add(new EmployeeBean(4,"Ram",20000.0F));
//		list.add(new EmployeeBean(5,"Vikas",15000.0F));
//		list.add(new EmployeeBean(6,"Rohit",25000.0F));
//		list.add(new EmployeeBean(7,"Aman",10000.0F));
//		list.add(new EmployeeBean(8,"Priyam",15000.0F));
//		list.add(new EmployeeBean(9,"Sidhant",25000.0F));
//		list.add(new EmployeeBean(10,"Sudarshar",20000.0F));
//		list.add(new EmployeeBean(11,"Rajat",10000.0F));
//		list.add(new EmployeeBean(12,"Chinmay",15000.0F));
		
		List<EmployeeBean> list = mockeDAO.readData();
		
		float expected = 195000.0f;
		float actual = new EmployeeDAO().getTotSal(list);
		
		Assert.assertEquals(expected, actual, 1e15);
		
		
	}
	
	@Test
	public void readDataTest() {
		
		List<EmployeeBean> list = new ArrayList<EmployeeBean>();
		list.add(new EmployeeBean(1,"John",10000.0F));
		list.add(new EmployeeBean(2,"Adam",20000.0F));
		list.add(new EmployeeBean(3,"Rahul",10000.0F));
		list.add(new EmployeeBean(4,"Ram",20000.0F));
		list.add(new EmployeeBean(5,"Vikas",15000.0F));
		list.add(new EmployeeBean(6,"Rohit",25000.0F));
		list.add(new EmployeeBean(7,"Aman",10000.0F));
		list.add(new EmployeeBean(8,"Priyam",15000.0F));
		list.add(new EmployeeBean(9,"Sidhant",25000.0F));
		list.add(new EmployeeBean(10,"Sudarshar",20000.0F));
		list.add(new EmployeeBean(11,"Rajat",10000.0F));
		list.add(new EmployeeBean(12,"Chinmay",15000.0F));
		
		boolean flag = true;
		for(int i=0; i<list.size(); i++) {
			if(!(list.get(i).equals(new EmployeeDAO().readData().get(i)))) {
				flag = false;
				break;
			}
		}
		
		Assert.assertEquals(true, flag);
		
	}

}