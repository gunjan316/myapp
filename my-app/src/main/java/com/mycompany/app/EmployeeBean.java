package com.mycompany.app;

public class EmployeeBean {

	private int id;
	
	private String name;
	
	private float salary;

	public EmployeeBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public EmployeeBean(int id, String name, float salary) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}


	@Override
	public String toString() {
		return "EmployeeBean [id=" + id + ", name=" + name + ", salary=" + salary + "]";
	}


	@Override
	public boolean equals(Object obj) {
		EmployeeBean emp = (EmployeeBean) obj;
		
		return emp.name.equals(name) && emp.salary==salary && emp.id==id;
	}
	
	
	
}
