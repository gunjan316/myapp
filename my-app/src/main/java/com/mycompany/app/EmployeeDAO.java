package com.mycompany.app;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeDAO {
	
	public List<EmployeeBean> readData(){
		
		List<EmployeeBean> list = new ArrayList<EmployeeBean>();
		EmployeeBean emp = null;
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader("Test1.csv"));
			String line;
			
			while((line = br.readLine())!= null) {
				String[] fields = line.split(",");
				emp = new EmployeeBean(Integer.parseInt(fields[0]), fields[1], Float.parseFloat(fields[2]));
				list.add(emp);
			}
		} catch (FileNotFoundException e) {
			System.out.println("FileNotFoundException");
		} catch (NullPointerException e) {
			System.out.println("NullPointerException");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
		
	}
	
	public float getTotSal(List<EmployeeBean> list) {

		return (float) list.stream().mapToDouble(EmployeeBean::getSalary).sum();
	}
	
	public EmployeeBean getEmployee(int id) {
		List<EmployeeBean> list = readData();
		for(int i=0; i<list.size(); i++) {
			if(list.get(i).getId()==id) {
				return list.get(i);
			}
		}
		return null;
	}

	
	public int getCount(List<EmployeeBean> list, float salary) {
		return list.stream().filter(emp -> emp.getSalary() == salary).collect(Collectors.toList()).size();
	}
	
}
